
public class IceCream {
  private String flavorIceCream;
  private int numScoops;
  private String typeIceCream;
  
  //constructor to initialize the object
 public IceCream(String flavorIceCream, int numScoops, String typeIceCream){

   this.flavorIceCream = flavorIceCream;
 this.numScoops = numScoops;
 this.typeIceCream = typeIceCream;
 }

   //getter and setter methods 
  public String getFlavorIceCream(){
   return flavorIceCream; 
  }
   
  public int getNumScoops(){
   return numScoops; 
  }
  
      public String getTypeIceCream(){
   return typeIceCream; 
  }       
      
  public void setFlavorIceCream(String flavor){
   this.flavorIceCream = flavor; 
  }
     
  public void setNumScoops(int num){
   this.numScoops = num; 
  }

  public void setTypeIceCream(String type){
    this.typeIceCream = type; 
  }
  
//an instance method that checks if the flavourIceCream is a Pistachio
 public boolean bestSeller() {
  if (flavorIceCream.equals("Pistachio")) {
   return true;
  } else {
   return false;
  }
 }
}






















