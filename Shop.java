import java.util.Scanner;
public class Shop {

 public static void main(String[] args) {

  IceCream iceArr[] = new IceCream[4];

 Scanner sc = new Scanner(System.in);

  for (int i = 0; i < iceArr.length; i++) {

   // takes input for each object
   System.out.println("What flavour do you want ? #" + (i + 1));
   String flavorIceCream = sc.nextLine();

   System.out.println("How many scoops ? #" + (i + 1));
   int numScoops = Integer.parseInt(sc.nextLine());

   System.out.println("What type of ice cream is it ? (vegan, dairy, sorbet)#" + (i + 1));
   String typeIceCream = sc.nextLine();

   //create a new object and place it in an array
  iceArr[i] = new IceCream(flavorIceCream, numScoops, typeIceCream);

  System.out.println();

 }
  
  System.out.print("Add new type of ice cream: ");
String newType = sc.nextLine();


  
  // Print last product before setter call
System.out.println("The following is an array before setter call");
  System.out.println("The person chose a "+ iceArr[3].getNumScoops() + "-scoop " + iceArr[3].getFlavorIceCream() + " flavor that is a " 
                       + iceArr[3].getTypeIceCream() + "-type ice cream." );
  System.out.println();
  iceArr[3].setTypeIceCream(newType);

  
  // Print last product after setter call
  System.out.println("The following is an array after setter call");
  System.out.println("The person chose a "+ iceArr[3].getNumScoops() + "-scoop " + iceArr[3].getFlavorIceCream() + " flavor that is a " + iceArr[3].getTypeIceCream()
    + "-type ice cream." );

  // call instance method from IceCream class
  if (iceArr[3].bestSeller()) {
   System.out.println("You chose our best seller");
  } else {
   System.out.println("Ayt");
  }

 }

}
